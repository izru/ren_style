var postcss = require('gulp-postcss');
var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var csso = require('gulp-csso');
var precss = require('precss');
var concat = require('gulp-concat');
var ftp = require('gulp-ftp');
var csslint = require('gulp-csslint');
var config = require('./config.js');
// Работа со стилями
gulp.task('css', function () {
    var processors = [
        autoprefixer(),
        precss()
    ];
    return gulp.src('./dev/*.css')
        .pipe(postcss(processors))
        .pipe(csso())
        .pipe(concat('all.css'))
        .pipe(gulp.dest('./public'));
});


// Загрузка на ФТП
gulp.task('ftp', function() {
		return gulp.src('public/*')
			.pipe(ftp(config))
});


gulp.task('watch', function() {
	gulp.watch('./dev/*.css', ['css', 'ftp']);
});



gulp.task('default', ['css', 'watch', 'ftp']);